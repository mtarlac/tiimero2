<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactMessage extends Mailable
{
   use Queueable, SerializesModels;

   public $contactMessage;
   public $emailAddress;
   /**
    * Create a new message instance.
    *
    * @return void
    */
   public function __construct($contactMessage)
   {
       $this->contactMessage = $contactMessage;
       $this->emailAddress = $contactMessage['emailAddress'];
   }
   /**
    * Build the message.
    *
    * @return $this
    */
   public function build()
   {
       //dd(env('MAIL_FROM_ADDRESS'));
       //dd(env('MAIL_SEND_TO'));
       //return $this->view('view.name');
       return $this->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
           ->to(env('MAIL_SEND_TO'))
           ->subject('Tiimero contact form')
           ->replyTo($this->emailAddress)
           ->view('emails.contact-message');
   }
}