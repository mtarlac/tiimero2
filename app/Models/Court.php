<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Playground;
use App\Models\CourtStatus;


class Court extends Model
{
    protected $table = 'courts';

    protected $fillable = ['playgrounds_id', 'name', 'description', 'court_statuses_id'];

    protected $hidden = [];

    public $timestamps = false;

    public function playgrounds() {
        return $this->belongsTo(Playground::class, 'playgrounds_id');
    } 
    
    public function court_statuses(){ 
        return $this->belongsTo(CourtStatus::class, 'court_statuses_id');
    }

    public function has_events() {
        return $this->belongsToMany(SportEvent::class, 'sport_events_has_court', 'courts_id', 'sport_events_id');
    }
}
