<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventStatus extends Model
{   
    protected $table = 'event_statuses';

    protected $fillable = ['name'];

    protected $hidden = [];

    public function sport_events() {
        return $this->hasMany(SportEvent::class, 'event_statuses_id');
    }
}
