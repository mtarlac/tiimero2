<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class SportEventHasCourt extends Pivot
{
    protected $table = 'sport_events_has_courts';

    public $timestamps = false;
    
    protected $fillable = ['sport_events_id', 'courts_id'];
}
