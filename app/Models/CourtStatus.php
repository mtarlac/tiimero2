<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourtStatus extends Model
{
    protected $table = 'court_statuses';

    protected $fillable = ['name'];

    protected $hiddeen = [];

    public function courts() { 
        return $this->hasMany(Court::class,'court_statuses_id');
    }
}
