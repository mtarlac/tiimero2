<?php

namespace App\Models;

use App\Models\User;
use App\Models\City;
use App\Models\PlaygroundStatus;
use Illuminate\Database\Eloquent\Model;

class Playground extends Model
{   
    protected $table = 'playgrounds';

    protected $hidden = [];

    public $timestamps = false;
    
    protected $fillable = [
       'lat',
       'lng',
       'created_at',
       'updated_at',
       'users_id',
       'name',
       'address',
       'cities_id',
       'description',
       'playground_statuses_id',
       'main_image'
    ];

    public function users() {
        return $this->belongsTo(User::class, 'users_id');
    }

    public function cities() {
        return $this->belongsTo(City::class, 'cities_id');
    }

    public function playground_statuses() {
        return $this->belongsTo(PlaygroundStatus::class, 'playground_statuses_id');
    }

    public function courts() {
        return $this->hasMany(Court::class, 'playgrounds_id', 'id');
    }
}

