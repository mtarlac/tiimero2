<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;;
use App\Models\User;
use App\Models\EventStatus;
use App\Models\SportType;
use App\Models\EventType;

class SportEvent extends Model
{
    protected $table = "sport_events";
    
    protected $fillable = [
        'starts_at', 
        'ends_at', 
        'name', 
        'description', 
        'no_of_players', 
        'users_id', 
        'open_slots', 
        'price', 
        'created_at', 
        'updated_at', 
        'event_statuses_id', 
        'sport_types_id',
        'event_types_id'
    ];

    protected $hidden = [];

    public function  users() {
        return $this->belongsTo(User::class, 'users_id');
    }

    public function event_statuses() {
        return $this->belongsTo(EventStatus::class, 'event_statuses_id');
    }

    public function sport_types() {
        return $this->belongsTo(SportType::class, 'sport_types_id');
    }

    public function event_types () {
        return $this->belongsTo(EventType::class, 'event_types_id');
    }

    public function have_users() {
        return $this->belongsToMany(User::class, 'sport_events_have_users', 'sport_events_id', 'users_id');
    }

    public function has_courts() {
        return $this->belongsToMany(Court::class, 'sport_events_has_courts', 'sport_events_id', 'courts_id');
    }
}
