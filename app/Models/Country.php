<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table ='countries';

    protected $fillable = ['name', 'alpha2', 'alpha3'];
    
    protected $hidden = [];
    
    public function cities() {
        return $this->hasMany(City::class, 'countries_id');
    }
}
