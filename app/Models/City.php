<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'cities';

    protected $fillable = ['name', 'countries_id', 'postal_code'];
    
    protected $hidden = [];

    public function countries() {
        return $this->belongsTo(Country::class, 'countries_id');
    }
    
    public function playgrounds(){
        return $this->hasMany(Playground::class, 'cities_id');
    }
}
