<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SportType extends Model
{   
    protected $table = 'sport_types';

    protected $fillable = ['name', 'no_of_players'];

    protected $hidden = []; 
    
    public function sport_events() {
        return $this->hasMany(SportEvent::class, 'sport_types_id');
    }
}
