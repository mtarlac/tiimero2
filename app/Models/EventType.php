<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventType extends Model
{
    protected $table = 'event_types';

    protected $fillable = ['name'];

    protected $hidden = [];

    public function sport_events() {
        return $this->hasMany(SportEvent::class, 'event_types_id');
    }
}
