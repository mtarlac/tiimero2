<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot; 

class SportEventHaveUser extends Pivot
{
    protected $table = 'sport_events_have_users';

    public $timestamps = false;
    
    protected $fillable = ['sport_events_id', 'users_id', 'attended'];
}
