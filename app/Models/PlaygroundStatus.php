<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlaygroundStatus extends Model
{
    protected $table = 'playground_statuses';

    protected $fillable = ['name'];
    
    protected $hidden = [];

    public function playgrounds() {
        return $this->hasMany(Playground::class, 'playground_statuses_id');
    }
}
