<?php

namespace App\Http\Controllers;

use App\Models\Court;
use App\Models\Playground;
use App\Models\CourtStatus;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use Illuminate\Http\Request;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function showProfile()
    {
        $user = Auth::user();

        //$playground = Playground::all();

        $playgrounds = Playground::where('users_id', $user->id)->paginate(5);

        return view("/front-pages.profile", compact('user','playgrounds'));
    }

    public function updateProfile(Request $request)
    {
        $user = Auth::user();

        if ($request->get('name') == "") {
            return redirect('/profile')->with('error', 'Name is required field!');
        }

        $user->name = $request->get('name');

        //$user->email = $request->get('email');
        
        $user->save();

        return redirect('/profile')->with('status', 'Your profile has been successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function destroy($id)
    {
        $user = Auth::user();

        $playground = Playground::where('users_id', $id)->first();

        $court = Court::where('playgrounds_id', $playground->id)->first();

        if ($user->user_types_id == 1 || $playground->users_id == $user->id) {

            $playground->delete();

            $court->delete();

            return redirect(route('home.index'));
        } 
        else {
            return abort(403);
        }
    }*/

    public function changePassword()
    {
        return view('/front-pages.change-password');
    }

    public function updatePassword(Request $request)
    {
        $request->validate([
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
        ]);

        User::find(auth()->user()->id)->update(['password' => Hash::make($request->new_password)]);
        
        return redirect('/home');
    }
}
