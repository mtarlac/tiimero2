<?php

namespace App\Http\Controllers;

use App\Models\Court;
use App\Models\Playground;
use App\Models\CourtStatus;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\Middleware\ShareErrorsFromSession;

class CourtController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
    {
         $this->middleware('auth', ['except'=>['index', 'show']]);
    }
    
    public function index()
    {   
        $courts = Court::with('playgrounds')
        ->with('court_statuses')
        ->paginate(15);

        return view('courts.index', compact('courts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $user = Auth::user();

        $playgrounds = ($user->user_types_id == 1) ? (Playground::all()):(Playground::where('users_id', $user->id)->get());
    
        $court_statuses = CourtStatus::all();

        return view('courts.create', compact('playgrounds', 'court_statuses', 'user'));
    }
        
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {  
        $validator = Validator::make($request->all(),[
            'playgrounds_id' => 'required|integer',
            'name' => 'required',
            'description' => 'required',
            'court_statuses_id' => 'required|integer'
        ])->validate();

        $playground = Playground::find($request->input('playgrounds_id'));

        $user = $request->user();
     
        if ($user->user_types_id == 1 || $playground->users_id == $user->id) {
        
            $data = $request->only(['playgrounds_id', 'name', 'description', 'court_statuses_id']);
            
            $court= Court::create($data); 

            return redirect('/courts/'.$court->id);
        }
        else {
            return abort(403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $court = Court::findOrFail($id);

        return view('courts.view', compact('court'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();

        $court = Court::find($id);

        $playground = Playground::findOrFail($court->playgrounds_id);
    
        if ($user->user_types_id == 1 || $playground->users_id == $user->id) {

            //$playground = ($user->user_types_id == 1) ? Playground::all():Playground::where('users_id', $user->id)->get();
        
            $court_statuses = CourtStatus::all();
    
            return view('courts.edit', compact('court', 'playground', 'court_statuses', 'user'));
        }
        else {
            return abort(403);
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'playgrounds_id' => 'required|integer',
            'name' => 'required',
            'description' => 'required',
            'court_statuses_id' => 'required|integer'
        ])->validate();

        $user = $request->user();

        $court = Court::findOrFail($id);

        $playground = Playground::findOrFail($court->playgrounds_id);

        if ($user->user_types_id == 1 || $playground->users_id == $user->id) {

            $court_statuses = CourtStatus::all();
    
            $data = $request->only(['playgrounds_id', 'name', 'description', 'court_statuses_id']);
        
            $court->update($data);
       
            return redirect('/courts/'.$court->id);
        }
        else {
            return abort(403);
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();

        $court = Court::find($id);
        
        $playground = Playground::find($court->playgrounds_id);

        if ($user->user_types_id == 1 || $playground->users_id == $user->id) {
            
            $court = Court::findOrFail($id)->delete();

            return redirect(route('courts.index'));
        } 
        else {
            return abort(403);
        }  
    }
}
