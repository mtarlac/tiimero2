<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactMessage;
use App\Models\Playground;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'showPage', 'sendMessage']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('welcome');
    }

    /**
     * Links to other home pages with $slug
     */
    public function showPage($slug)
    {
        $user = Auth::user();

        if (view()->exists('front-pages') . $slug) {
            return view('front-pages.' . $slug, compact('user'));
        } else {
            return view('front-pages.404', compact('user'));
        }
    }

    public function sendMessage(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'yourName' => 'required',
            'emailAddress' => 'required|email',
            'textBox' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        Mail::send(new ContactMessage($request->all()));

        return response()->json(['message' => 'Message sent successfully'], 200);
    }
}
