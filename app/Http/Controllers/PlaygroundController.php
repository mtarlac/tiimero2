<?php

namespace App\Http\Controllers;

use App\Models\PlaygroundStatus;
use App\Models\User;
use App\Models\Playground;
use App\Models\City;
use App\Models\Court;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class PlaygroundController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    public function index()
    {
        $playgrounds = Playground::with('cities')
        ->with('playground_statuses')
        ->with('users')
        ->paginate(15);

        return view('playground.index', compact('playgrounds'));
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $user = Auth::user();

        $playground_statuses = PlaygroundStatus::all();

        $cities = City::all();
        
        return view('playground.create', compact('user', 'playground_statuses', 'cities'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|voids
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'lat' => 'required',
            'lng' => 'required',
            'name' => 'required',
            'address' => 'required',
            'cities_id' => 'required|integer',
            'description' => 'required',
            'playground_statuses_id' => 'required|integer'
        ])->validate();

        $user = $request->user();

        $data = $request->only(['lat', 'lng', 'name', 'address', 'cities_id', 'description', 'playground_statuses_id']);

        $data['users_id'] = $user->id;

        $playground = Playground::create($data);

        return redirect('/playgrounds/' . $playground->id);
    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $playground = Playground::findOrFail($id);

        return view('playground.view', compact('playground'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $user = Auth::user();

        $playground = Playground::findOrFail($id);

        $playground_statuses = PlaygroundStatus::all();

        $cities = City::all();

        if($user->user_types_id == 1) {

            $courts = Court::with('playgrounds')
                    ->with('court_statuses')
                    ->paginate(15);

            return view('playground.edit', compact('user', 'playground', 'playground_statuses', 'cities', 'courts'));
        }
        else {

            $playgroundIds = Playground::where('users_id', $user->id)->pluck('id')->toArray();

            $playgrounds = Playground::whereIn('id', $playgroundIds)->get();

            $courts = Court::whereIn('id', $playgroundIds)->paginate(5);

            return view ('playground.edit', compact('user', 'playground', 'playground_statuses', 'playgroundIds', 'playgrounds', 'cities', 'courts'));
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'lat' => 'required',
            'lng' => 'required',
            //'users_id' => 'required|integer',
            'name' => 'required',
            'address' => 'required',
            'cities_id' => 'required|integer',
            'description' => 'required',
            'playground_statuses_id' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $user = Auth::user();

        $playground = Playground::findOrFail($id);

        if ($user->user_types_id == 1 || $playground->users_id == $user->id) {

            $data = $request->only(['lat', 'lng', 'name', 'address', 'cities_id', 'description', 'playground_statuses_id']);

            $playground->update($data);

            return redirect('/playgrounds/' . $playground->id);
        } else {
            return abort(403);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     */
    public function destroy($id)
    {
        $user = Auth::user();

        $playground = Playground::findOrFail($id);

        if ($user->user_types_id == 1 || $playground->users_id == $user->id) {

            $courts = Court::Where('playgrounds_id', $playground->id)->delete();

            $playground->delete();

            return redirect(route('playgrounds.index'));
        } else {
            return abort(403);
        }
    }
}
