<?php

namespace App\Http\Controllers;

use App\Models\SportEvent;
use App\Models\User;
use App\Models\EventStatus;
use App\Models\SportType;
use App\Models\EventType;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use DB;

class SportEventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    public function index()
    {
        $user = Auth::user();

        $userEvents = DB::table('sport_events_have_users')
            ->where('users_id', $user->id)
            ->pluck('sport_events_id')->toArray();

        $events = SportEvent::with('event_statuses')
                            ->with('sport_types')
                            ->with('event_types')
                            ->with('users')
                            ->paginate(15);

        return view('events.index', compact('events', 'user', 'userEvents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();

        $event_statuses = EventStatus::all();

        $sport_types = SportType::all();

        $event_types = EventType::all();

        return view('events.create', (compact('user', 'event_statuses', 'sport_types', 'event_types')));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'starts_at' => 'required', 
            'ends_at' => 'required',  
            'name' => 'required',  
            'description' => 'required', 
            'no_of_players' => 'required|integer',  
            'open_slots' => 'required|integer',  
            'price' => 'required', 
            'event_statuses_id' => 'required|integer', 
            'sport_types_id' => 'required|integer', 
            'event_types_id' => 'required|integer' 
        ])->validate();

        $user = $request->user();

        if ($user->user_types_id == 1 || $user->id) {

            $data = $request->only(['starts_at', 'ends_at', 'name', 'description', 'no_of_players', 'users_id', 'open_slots', 'price', 'event_statuses_id', 'sport_types_id', 'event_types_id']);

            $data['users_id'] = $user->id;

            $event = SportEvent::create($data);

            //dd($sport_event);

            return redirect('/events/' . $event->id);
        } else {
            return abort(403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = SportEvent::findOrFail($id);

        return view('events.view', compact('event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { 
        $event = SportEvent::findOrFail($id);
        $event_statuses = EventStatus::all();
        $sport_types = SportType::all();
        $event_types = EventType::all();
       
        return view('events.edit', compact('event', 'event_statuses', 'sport_types', 'event_types'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $validator = Validator::make($request->all(), [
            'starts_at' => 'required',
            'ends_at' => 'required',
            'name' => 'required',
            'description' => 'required',
            'no_of_players' => 'required|integer',
            'open_slots' => 'required|integer',
            'price' => 'required',
            'event_statuses_id' => 'required|integer',
            'sport_types_id' => 'required|integer',
            'event_types_id' => 'required|integer'
        ])->validate();

        $user = Auth::user();

        $event = SportEvent::findOrFail($id);

        if ($user->user_types_id == 1 || $event->users_id == $user->id) {

            $data = $request->only(['starts_at', 'ends_at', 'name', 'description', 'no_of_players', 'users_id', 'open_slots', 'price', 'event_statuses_id', 'sport_types_id', 'event_types_id']);

            $event->update($data);

            return redirect('/events/' . $event->id);
        } else {
            return abort(403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();

        $event = SportEvent::findOrFail($id);

        if($user->user_types_id == 1 || $event->users_id == $user->id) 
        {

            $event = SportEvent::findOrFail($id)->delete();

            return redirect(route('events.index'));
        }
        else {
            return abort(403);
        }
    }


    public function joinLeaveEvent(Request $request, $id) 
    {
        $user = Auth::user();
        $event = SportEvent::findOrFail($id);        
        $joined = DB::table('sport_events_have_users')
                            ->where('users_id', $user->id)
                            ->where('sport_events_id', $event->id)
                            ->first();
       
        if ($joined == null) {            
            $join = DB::table('sport_events_have_users')
                            ->insert(['users_id' => $user->id, 'attended'=> 0, 'sport_events_id' => $event->id]);
            //return response()->json(['message' => 'You have successfully joined the Sport Event!'], 200);
            return response()->json(['message' => 1], 200);
        }
        else {

            $leave = DB::table('sport_events_have_users')
                            ->where('users_id', $user->id)
                            ->where('sport_events_id', $event->id)
                            ->delete();
            //return response()->json(['message' => 'You have successfully left the Sport Event!'], 200);
            return response()->json(['message' => 0], 200);
        }

    }

}