CREATE TABLE IF NOT EXISTS `playgrounds` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `lat` DECIMAL(10,6) NOT NULL,
  `lng` DECIMAL(10,6) NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT '2000-01-01 00:00:00',
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `users_id` INT(11) NOT NULL COMMENT 'was user_id',
  `name` VARCHAR(255) NOT NULL,
  `address` VARCHAR(255) NOT NULL,
  `cities_id` INT(11) NOT NULL COMMENT 'was cities_id',
  `description` VARCHAR(255) NULL DEFAULT NULL,
  `playground_statuses_id` INT NOT NULL,
  `main_image` VARCHAR(255) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_playgrounds_users1_idx` (`users_id` ASC),
  INDEX `fk_playgrounds_cities1_idx` (`cities_id` ASC),
  INDEX `fk_playgrounds_playground_statuses1_idx` (`playground_statuses_id` ASC),
  CONSTRAINT `fk_playgrounds_cities1`
    FOREIGN KEY (`cities_id`)
    REFERENCES `cities` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_playgrounds_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_playgrounds_playground_statuses1`
    FOREIGN KEY (`playground_statuses_id`)
    REFERENCES `playground_statuses` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;



INSERT INTO `playground_statuses` (`id`, `name`) VALUES
(1, 'Open'),
(2, 'Closed'),
(3, 'Under construction');

INSERT INTO `sport_types` (`id`, `name`, `no_of_players`) VALUES
(1, 'Basketball', 6),
(2, 'Tennis', 2),
(3, 'Table tennis', 2),
(4, 'Baseball', 6),
(5, 'Frisbee', 2),
(6, 'Mountaineering', 10),
(7, 'Hiking', 10),
(8, 'Cycling', 5),
(9, 'Snooker', 2),
(10, 'Fishing', 2),
(11, 'Running', 2),
(12, 'Skiing', 2),
(13, 'Rafting', 6),
(14, 'Chess', 2),
(15, 'Football', 10);

INSERT INTO `court_statuses` (`id`, `name`) VALUES
(1, 'Open'),
(2, 'Closed'),
(3, 'Under construction');

INSERT INTO `event_statuses` (`id`, `name`) VALUES
(1, 'Open'),
(2, 'Cancelled'),
(3, 'Expired'),
(4, 'Finished');

INSERT INTO `event_types` (`id`, `name`) VALUES
(1, 'Public'),
(2, 'Invitation only'),
(3, 'Closed');