ALTER TABLE `users` 
ADD COLUMN `user_types_id` INT(11) NOT NULL DEFAULT 1 AFTER `updated_at`,
ADD COLUMN `active` TINYINT(1) NOT NULL DEFAULT 1 AFTER `user_types_id`,
ADD COLUMN `receive_news` TINYINT(1) NOT NULL DEFAULT 1 AFTER `active`,
ADD COLUMN `date_of_birth` DATE NULL AFTER `receive_news`,
ADD COLUMN `activated` TINYINT(1) NOT NULL DEFAULT 0 AFTER `date_of_birth`;
