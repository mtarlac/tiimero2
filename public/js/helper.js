/**
 * Validates required fields
 * @param fields
 * @returns {boolean}
 */
function nValidatorReq(fields) {
    var valid = true;

    for (i = 0; i < fields.length; i++) {
        var field = fields[i];
        var value = $("#" + field).val();
        if (value === "") {
            $("#" + field).addClass("notAvailable");
            valid = false;
        } else {
            $("#" + field).removeClass("notAvailable");
        }
    }

    return valid;
}

/**
 * Validates that field is greater then zero
 * @param fields
 * @returns {boolean}
 */
function nValidatorGTZero(fields) {
    var valid = true;

    for (i = 0; i < fields.length; i++) {
        var field = fields[i];
        var value = document.getElementsByName(field)[0].value;
        if (value < 1) {
            $("#" + field).addClass("notAvailable");
            valid = false;
        } else {
            $("#" + field).removeClass("notAvailable");
        }
    }

    return valid;
}

/**
 * Validate min and max values in field
 * @param field
 * @param min
 * @param max
 * @returns {boolean}
 */
function nValidatorBetween(field, min, max) {
    var valid = true;
    var value = document.getElementsByName(field)[0].value;
    var a = parseInt(value);
    if (a < min || a > max || isNaN(a)) {
        $("#" + field).addClass("notAvailable");
        valid = false;
    } else {
        $("#" + field).removeClass("notAvailable");
    }

    return valid;
}


function joinLeaveEvent(id) {
    console.log("Kliknuli smo na dugme " + id);
    $.ajax({
        type: "POST",
        url: "/event/" + id,
        data: {
            "_token": document.getElementsByName("_token")[0].value
        },
        dataType: "json",
        success: function (data) {
            if (data.message == "0") {
                $("#buttonEvent" + id).html('Join');
                $("#messageDiv").html('<br/>You have successfully left the Sport Event!<br/>');
                $("#buttonEvent" + id).addClass("btn-primary");
                $("#buttonEvent" + id).removeClass("btn-danger");
            }
            else {
                $("#buttonEvent" + id).html('Leave');
                $("#messageDiv").html('<br/>You have successfully joined the Sport Event!<br/>');
                $("#buttonEvent" + id).removeClass("btn-primary");
                $("#buttonEvent" + id).addClass("btn-danger");
            }
        },
        error: function (data) {
            /*var htmlVar = '';
            var errors = data.responseJSON;
            $("#messageDiv").html('');
            $("#messageDiv").html(htmlVar);*/
        }
    });
    return false;
}