<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('welcome');


//login
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//showPages
Route::get('/page/{slug}', 'HomeController@showPage');

//contactMessage to User
Route::post('/sendMessage', 'HomeController@sendMessage');

//playgruond
Route::resource('/playgrounds', 'PlaygroundController');

//courts
Route::resource('/courts', 'CourtController');

//events
Route::resource('/events', 'SportEventController');
//Route::post('/joinLeaveEvent', 'SportEventController@joinLeaveEvent');
Route::post('/event/{id}', 'SportEventController@joinLeaveEvent')->name('join.event');

//admin/profile/change-password
Route::get('/profile', 'ProfileController@showProfile');
Route::put('/profile', 'ProfileController@updateProfile')->name('update.profile');


Route::get('/password', 'ProfileController@changePassword');
Route::post('/password', 'ProfileController@updatePassword')->name('change.password');
