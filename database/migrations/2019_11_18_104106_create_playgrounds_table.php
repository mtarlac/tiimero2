<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlaygroundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        //Schema::disableForeignKeyConstraints();

        Schema::create('playgrounds', function (Blueprint $table) {
            $table->bigIncrements('id', 11);
            $table->decimal('lat', 10.6);
            $table->decimal('lat', 10.6);
            $table->timestamps();
            $table->string('name');
            $table->string('address');
            $table->bigInteger('cities_id', 11);
            $table->string('description');
            $table->bigInteger('playground_statuses_id');

            $table->foreign('cities_id')->references('id')->on('cities');
            $table->foreign('playground_statuses_id')->references('id')->on('playground_statuses'); 
        });
        //Schema::enableForeignKeyConstraints();

        Schema::create('playground_statuses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('name', 45);
        });

        Schema::create('countries', function (Blueprint $table){
            $table->bigIncrements('id');
            $table->string('name');
            $table->char('alpha2', 2);
            $table->char('alpha3', 3);
        });

        Schema::create('cities', function (Blueprint $table){
            $table->bigIncrements('id');
            $table->string('name');
            $table->bigInteger('countries_', 11);
            $table->char('postal_code', 25);

            $table->foreign('countries_id')->references('id')->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('playgrounds');
        Schema::dropIfExists('playground_statuses');
        Schema::dropIfExists('countries');
        Schema::dropIfExists('cities');
    }
}
