<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('partials.head')

<body id="body" class="up-scroll">

@include('partials.preloader')

<!-- HEADER -->
<header class="header">
    <nav class="nav-menuzord nav-menuzord-transparent navbar-sticky">
        <div class="container clearfix">
            @include('partials.menu')
        </div>
    </nav>
</header>

<div class="main-wrapper">

    @yield('content')

</div>
<!-- element wrapper ends -->

<!-- Footer -->
@include('partials.footer')

<!-- JAVASCRIPTS -->
@include('partials.javascript')

@yield('footerJS')

</body>

</html>
