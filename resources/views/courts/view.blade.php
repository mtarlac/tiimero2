@extends('layouts.page')

@section('caption', 'Court')

@section('content')

<!-- ====================================
———	LISTING SINGLE HEADING
===================================== -->
<section class="py-7 pt-md-9 pb-md-8">
  <div class="container">
    <div class="d-md-flex flex-column flex-md-row justify-content-md-between align-items-end">
      <div class="mb-2 mb-md-0">
        <h1 class="single-listing-title"> {{ $court->name }}</h1>
        <p>Court Name</p>
      </div>
    </div>
  </div>
</section>

<!-- ====================================
———	LISTING DETAILS CAROUSEL
===================================== -->
<section>
  <div class="owl-carousel owl-theme listing-details-carousel nav-light-dark">

    <div class="single-item">
      <a class="overlay-dark" href="/assets/img/single-listing/store/store-img-big-01.jpg" data-fancybox="gallery"
        data-caption="Caption for single image">
        <img src="/assets/img/single-listing/store/store-img-01.jpg" alt="Image" />
      </a>
    </div>

    <div class="single-item">
      <a class="overlay-dark" href="/assets/img/single-listing/store/store-img-big-02.jpg" data-fancybox="gallery"
        data-caption="Caption for single image">
        <img src="/assets/img/single-listing/store/store-img-02.jpg" alt="Image" />
      </a>
    </div>

    <div class="single-item">
      <a class="overlay-dark" href="/assets/img/single-listing/store/store-img-big-03.jpg" data-fancybox="gallery"
        data-caption="Caption for single image">
        <img src="/assets/img/single-listing/store/store-img-03.jpg" alt="Image" />
      </a>
    </div>

    <div class="single-item">
      <a class="overlay-dark" href="/assets/img/single-listing/store/store-img-big-04.jpg" data-fancybox="gallery"
        data-caption="Caption for single image">
        <img src="/assets/img/single-listing/store/store-img-04.jpg" alt="Image" />
      </a>
    </div>

    <div class="single-item">
      <a class="overlay-dark" href="/assets/img/single-listing/store/store-img-big-05.jpg" data-fancybox="gallery"
        data-caption="Caption for single image">
        <img src="/assets/img/single-listing/store/store-img-05.jpg" alt="Image" />
      </a>
    </div>

    <div class="single-item">
      <a class="overlay-dark" href="/assets/img/single-listing/store/store-img-big-06.jpg" data-fancybox="gallery"
        data-caption="Caption for single image">
        <img src="/assets/img/single-listing/store/store-img-06.jpg" alt="Image" />
      </a>
    </div>

    <div class="single-item">
      <a class="overlay-dark" href="/assets/img/single-listing/store/store-img-big-07.jpg" data-fancybox="gallery"
        data-caption="Caption for single image">
        <img src="/assets/img/single-listing/store/store-img-07.jpg" alt="Image" />
      </a>
    </div>

  </div>
</section>

<!-- ====================================
———	MAIN CONTENT
===================================== -->
<section class="pt-7 pb-4 pt-md-9 pb-md-8">
  <div class="container">
    <div class="row">
      <div class="col-md-7 col-lg-8">

        <!-- Listing Main Content -->
        <div class="single-listing-content mb-6">

          <a href="/playgrounds/{{ $court->playgrounds_id }}"><h3 class="font-weight-normal">{{ $court->playgrounds->name }}</h3></a>
          <p>Playground name</p>

          <h3 class="font-weight-normal">Description</h3>
          <p class="mb-6">{{ $court->description }}</p> 

          <!-- Departments at this Store -->
          <div class="mb-6">
            <h4 class="font-weight-normal mb-4">Court Status</h4>
            <p class="mb-1">{{ $court->court_statuses->name }}</p>
          </div>

        </div>

        <hr> 

      </div>

      <!--======= Sidebar =======-->
      <div class="col-md-5 col-lg-4 pl-xl-8">

        <!-- Single listing Map -->
        <div class="map-sidebar border rounded mb-5">
          <div id="single-listing-map" data-lat="40.705793" data-lag="-74.006207"></div>
          <div class="px-6 py-5">
            <ul class="list-unstyled mb-0">
              <li class="d-flex align-items-start">
                <i class="fa fa-map-marker mr-3 mt-1" aria-hidden="true"></i>
                <span>2880 Broadway, New York, NY 10025, USA</span>
              </li>
            </ul>
          </div>
        </div>

      </div>

    </div>
  </div>
</section>

  </div> <!-- element wrapper ends -->

@endsection