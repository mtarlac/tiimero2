@extends('layouts.page')

@section('caption', 'Courts')

@section('content')
<!-- ====================================
———	LISTING HALF MAP LIST
===================================== -->
<section class="main-contentiner map-half-content grid-two-items">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 order-lg-2 pl-lg-0">
        <div class="inner-container">
          <div class="map-lg-fixed">
            <div class="map-container">
              <div id="listing-main-map" class="map-half"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6 px-xl-6">
    
        <!-- List items goes here -->
            @foreach($courts as $court)
        <div class="card card-list card-listing" data-lat="-33.922125" data-lag="151.159277" data-id="1">
          <div class="row">
            <div class="col-sm-5 col-xl-4">
              <div class="card-list-img">
                <img class="listing-img" src="/assets/img/listing/listing-4.jpg" alt="things-4">
              </div>

            </div>
            <div class="col-sm-7 col-xl-8">
              <div class="card-body p-0">
                
                <div class="d-flex justify-content-between align-items-center mb-1">
                  <h3 class="card-title listing-title mb-0">
                    <a href="/courts/{{ $court->id }}"> {{ $court->name }} </a>
                  </h3>
                </div>

              </div>
                <a href="/playgrounds/{{ $court->playgrounds_id }}"> {{ $court->playgrounds->name }} </a>
                <p></p>
                <p class="mb-4"> {{ $court->description }} </p>
              <div>
                <button class="btn-like" data-toggle="tooltip" data-placement="top" title="Court Status">
                  <i class="text-primary" aria-hidden="true">{{ $court->court_statuses->name }}</i>
                </button>
              </div>
            </div>
          </div>
        </div>
            @endforeach

        {{ $courts->links() }}

</div> <!-- element wrapper ends -->

@endsection