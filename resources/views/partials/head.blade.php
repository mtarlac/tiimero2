<head>
    <!-- SITE TITTLE -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ env('APP_NAME') }} - @yield('caption')</title>

    <!-- PLUGINS CSS STYLE -->
    <link href="/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="/assets/plugins/listtyicons/style.css" rel="stylesheet">
    <link href="/assets/plugins/menuzord/css/menuzord.css" rel="stylesheet">
    <link href="/assets/plugins/map/css/map.css" rel="stylesheet">
    <link href="/assets/plugins/selectric/selectric.css" rel="stylesheet">
    <link href="/assets/plugins/dzsparallaxer/dzsparallaxer.css" rel="stylesheet">
    <link href="/assets/plugins/owl-carousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="/assets/plugins/owl-carousel/assets/owl.theme.default.min.css" rel="stylesheet">
    <link href="{{asset('assets/plugins/revolution/css/settings.css')}}" rel="stylesheet">
    <link href="/assets/plugins/map/css/map.css" rel="stylesheet">
    <link href="{{asset('assets/plugins/rateyo/jquery.rateyo.min.css')}}" rel="stylesheet">
    <link href="/assets/plugins/DataTables/DataTables-1.10.18/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="/assets/plugins/DataTables/Responsive-2.2.2/css/responsive.dataTables.min.css" rel="stylesheet">
    <link href="{{asset('assets/plugins/daterangepicker/daterangepicker.css')}}" rel="stylesheet">

    <link href="/assets/plugins/fancybox/jquery.fancybox.min.css" rel="stylesheet">
    <link href="/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">

    <!-- GOOGLE FONT -->
    <link href="https://fonts.googleapis.com/css?family=Muli:200,300,400,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

    <!-- CUSTOM CSS -->
    <link href="/assets/css/listty.css" rel="stylesheet" id="option_style">

    <!-- <link rel="stylesheet" href="/assets/css/default.css" id="option_color"> -->

    <!-- FAVICON -->
    <link href="/assets/img/favicon.png" rel="shortcut icon">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link href="/css/helper.css" rel="stylesheet">

</head>
