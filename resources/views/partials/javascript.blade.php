<script src="/assets/plugins/jquery/jquery-3.4.1.min.js"></script>

<script src="/assets/plugins/bootstrap/js/bootstrap.bundle.js"></script>
<script src="/assets/plugins/menuzord/js/menuzord.js"></script>
<script src="/assets/plugins/selectric/jquery.selectric.min.js"></script>
<script src="/assets/plugins/dzsparallaxer/dzsparallaxer.js"></script>
<script src="/assets/plugins/isotope/isotope.pkgd.min.js"></script>
<script src="/assets/plugins/daterangepicker/moment.min.js"></script>
<script src="/assets/plugins/daterangepicker/daterangepicker.js"></script>
<script src="/assets/plugins/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="/assets/plugins/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="/assets/plugins/owl-carousel/owl.carousel.min.js"></script>
<script src="/assets/plugins/rateyo/jquery.rateyo.min.js"></script>

<script src="/assets/plugins/DataTables/DataTables-1.10.18/js/jquery.dataTables.min.js"></script>
<script src="/assets/plugins/DataTables/Responsive-2.2.2/js/dataTables.responsive.min.js"></script>

<script src="/assets/plugins/fancybox/jquery.fancybox.min.js"></script>
<script src="/assets/plugins/imagesloaded/imagesloaded.pkgd.min.js"></script>


<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDU79W1lu5f6PIiuMqNfT1C6M0e_lq1ECY"></script>-->
<!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ env('MAPS_API_KEY') }}&callback=initMap"></script>-->
<script type="text/javascript" src='https://maps.google.com/maps/api/js?key={{ env('MAPS_API_KEY') }}&libraries=places'></script>
<script src="/assets/plugins/map/js/markerclusterer.js"></script>
<script src="/assets/plugins/map/js/rich-marker.js"></script>
<script src="/assets/plugins/map/js/infobox_packed.js"></script>
<script src="/assets/js/map.js"></script>
<!--<script src="/assets/plugins/fancybox/jquery.fancybox.min.js"></script>-->
<!-- Flot -->
<script src="/assets/plugins/flot/jquery.flot.js"></script>
<script src="/assets/plugins/flot/jquery.flot.time.js"></script>
<script src="/assets/js/chart.js"></script>

<!-- <script src="/assets/plugins/perfect-scrollbar/dist/perfect-scrollbar.js"></script> -->

<script src="/assets/js/listty.js"></script>


