@extends('layouts.page')

@section('caption', 'View')

@section('content')

<div class="main-wrapper">

    <!-- ====================================
———	LISTING SINGLE HEADING
===================================== -->
    <section class="py-7 pt-md-9 pb-md-8">
        <div class="container">
            <div class="d-md-flex flex-column flex-md-row justify-content-md-between align-items-end">
                <div class="mb-2 mb-md-0">
                    <h1 class="single-listing-title">{{ $playground->name }}</h1>
                    <p>Playgrounds Name</p>
                </div>
            </div>
        </div>
    </section>

    <!-- ====================================
———	LISTING DETAILS CAROUSEL
===================================== -->
    <section>
        <div class="owl-carousel owl-theme listing-details-carousel nav-light-dark">

            <div class="single-item">
                <a class="overlay-dark" href="/assets/img/single-listing/store/store-img-big-01.jpg" data-fancybox="gallery" data-caption="Caption for single image">
                    <img src="/assets/img/single-listing/store/store-img-01.jpg" alt="Image" />
                </a>
            </div>

            <div class="single-item">
                <a class="overlay-dark" href="/assets/img/single-listing/store/store-img-big-02.jpg" data-fancybox="gallery" data-caption="Caption for single image">
                    <img src="/assets/img/single-listing/store/store-img-02.jpg" alt="Image" />
                </a>
            </div>

            <div class="single-item">
                <a class="overlay-dark" href="/assets/img/single-listing/store/store-img-big-03.jpg" data-fancybox="gallery" data-caption="Caption for single image">
                    <img src="/assets/img/single-listing/store/store-img-03.jpg" alt="Image" />
                </a>
            </div>

            <div class="single-item">
                <a class="overlay-dark" href="/assets/img/single-listing/store/store-img-big-04.jpg" data-fancybox="gallery" data-caption="Caption for single image">
                    <img src="/assets/img/single-listing/store/store-img-04.jpg" alt="Image" />
                </a>
            </div>

            <div class="single-item">
                <a class="overlay-dark" href="/assets/img/single-listing/store/store-img-big-05.jpg" data-fancybox="gallery" data-caption="Caption for single image">
                    <img src="/assets/img/single-listing/store/store-img-05.jpg" alt="Image" />
                </a>
            </div>

            <div class="single-item">
                <a class="overlay-dark" href="/assets/img/single-listing/store/store-img-big-06.jpg" data-fancybox="gallery" data-caption="Caption for single image">
                    <img src="/assets/img/single-listing/store/store-img-06.jpg" alt="Image" />
                </a>
            </div>

            <div class="single-item">
                <a class="overlay-dark" href="/assets/img/single-listing/store/store-img-big-07.jpg" data-fancybox="gallery" data-caption="Caption for single image">
                    <img src="/assets/img/single-listing/store/store-img-07.jpg" alt="Image" />
                </a>
            </div>

        </div>
    </section>

    <!-- ====================================
———	MAIN CONTENT
===================================== -->
    <section class="pt-7 pb-4 pt-md-9 pb-md-8">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-lg-8">
					<!-- Listing Main Content -->
						<div class="single-listing-content mb-6">

                            <h3 class="font-weight-normal mb-6">Playgrounds Latitude</h3>
							<p class="mb-6">{{ $playground->lat }}</p>

                            <h3 class="font-weight-normal mb-6">Playgrounds Longitude</h3>
							<p class="mb-6">{{ $playground->lng }}</p>

							<h3 class="font-weight-normal mb-6">Description</h3>
							<p class="mb-6">{{ $playground->description }}</p>

							<!-- Departments at this Store -->
							<div class="mb-6">
								<h4 class="font-weight-normal mb-4">Playground Status</h4>
								<p class="mb-6">{{ $playground->playground_statuses->name }}</p>
							</div>

							<!-- Popular Brands at this Store -->
							<div class="mb-6">
								<h4 class="font-weight-normal mb-4">City</h4>
								<p class="mb-6">{{ $playground->cities->name }}</p>

								<h4 class="font-weight-normal mb-4">User</h4>
								<p class="mb-6">{{ $playground->users->email }}</p>
							</div>
						</div>
				</div>
				<hr>

				<!--======= Sidebar =======-->
				<div class="col-md-5 col-lg-4 pl-xl-8">
					<!-- Single listing Map -->
					<div class="map-sidebar border rounded mb-5">
						<div id="playgroundMap" style="full-width; height: 450px"></div>
							<div class="px-6 py-5">
								<ul class="list-unstyled mb-0">
									<li class="d-flex align-items-start">
										<i class="fa fa-map-marker mr-3 mt-1" aria-hidden="true"></i>
										<span>{{ $playground->address }}</span>
									</li>
								</ul>
							</div>
							
					</div>
				</div>

				
			</div>
        </div>
    </section>
    @endsection

</div> <!-- element wrapper ends -->

@section('footerJS')
    <script src="{{ url('') }}/js/locationpicker.jquery.js"></script>
    <script>
        $(document).ready(function () {
        //$(function () {

            var latitude = 44.765345;
            @if ($playground->lat !== null && $playground->lat != 0)
                latitude = parseFloat({{ $playground->lat }}).toFixed(6);
                @endif
            var longitude = 17.197233;
            @if ($playground->lng !== null || $playground->lng != 0)
                longitude = parseFloat({{ $playground->lng }}).toFixed(6);
            @endif

            $('#playgroundMap').locationpicker({
                location: {
                    latitude: latitude,
                    longitude: longitude
                },
                //markerInCenter: true,
                inputBinding: {
                    latitudeInput: $('#lat'),
                    longitudeInput: $('#lng')
                },
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [],
                mapOptions: {},
                radius: 0
            });

        });

    </script>
@endsection