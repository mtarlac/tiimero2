@extends('layouts.page')
 
@section('caption', 'Create/Store Playground')

@section('content')
    <nav class="navbar navbar-expand-md navbar-dark">
        <div class="container">

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link " href="dashboard-list-admin.html">
                            <i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard <span class="sr-only">(current)</span></a>
                    </li>

                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle active" href="javascript:void(0)" id="navbarDropdown"
                           role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-list-ul" aria-hidden="true"></i> Listing
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="my-listings.html">My Listing</a>
                            <a class="dropdown-item" href="add-listings.html">Add Listing</a>
                            <a class="dropdown-item" href="edit-listings.html">Edit Listing</a>
                        </div>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link " href="messages-list-admin.html">
                            <i class="fa fa-envelope" aria-hidden="true"></i> Messages <span
                                class="badge badge-success">12</span> </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link " href="admin-booking-requests.html">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Booking Request </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link " href="admin-reviews.html">
                            <i class="fa fa-eye" aria-hidden="true"></i> Reviews </a>
                    </li>

                </ul>

                <form class="form-inline my-2 my-lg-0 position-relative d-none d-md-block">
                    <input class="form-control-sm" placeholder="Search" aria-label="Search">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </form>
            </div>
        </div>
    </nav>


    <!-- ====================================
    ———	Add Listing
    ===================================== -->
    <section class="bg-light py-5 height100vh">
        <div class="container">
            <nav class="bg-transparent breadcrumb breadcrumb-2 px-0 mb-5" aria-label="breadcrumb">
                <h2 class="font-weight-normal mb-4 mb-md-0">Submit Listings</h2>
                <ul class="list-unstyled d-flex p-0 m-0">
                    <li class="breadcrumb-item"><a href="/home">Home</a></li>
                    <li class="breadcrumb-item"><a href="/profile">Dashboard</a></li>
                    <!-- <li class="breadcrumb-item active" aria-current="page">Submit Listings</li> -->
                </ul>
            </nav>

            <form action="{{ route('playgrounds.store') }}" method="POST">
            @csrf

            <!-- About -->
                <div class="card">
                    <div class="card-body px-6 pt-6 pb-1">
                        <h3 class="h4 mb-4">About</h3>
                        <p class="mb-5">We are not responsible for any damages caused by the use of this website, or by
                            posting business listings here. Please use our site at your own discretion and exercise good
                            judgement as well as common sense when advertising business here.</p>

                        <div class="row">

                            <div class="form-group col-md-6 mb-6">
                                <label for="playgroundName">Playground Name</label>
                                <input type="text" class="form-control" id="playgroundName" name="name"
                                       placeholder="Playground Name">
                            </div>

                            <div class="form-group col-md-6 mb-6">
                                <!-- <label for="userName">User</label>
                                <input type="text" class="form-control" id="userName" name="users_id" placeholder="User Name"> -->
                            </div>

                            <div class="form-group col-md-6 mb-6">
                                <label for="address">Address</label>
                                <input type="text" class="form-control" name="address" id="address"
                                       placeholder="Address">
                            </div>

                            <div class="form-group col-md-6 mb-6">
                                <label for="cities_id">Cities</label>
                                <div class="select-default bg-white">
                                    <select class="select-location" name="cities_id">
                                    
                                        @foreach($cities as $city)
                                            <option value="{{ $city->id }}">{{ $city->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group col-md-12 mb-6">
                                <label for="description">Description</label>
                                <textarea class="form-control" rows="6" name="description"
                                          placeholder="Description"></textarea>
                            </div>

                            <div class="form-group col-md-6 mb-6">
                                <label for="playground_statuses_id">Playground Status</label>
                                <div class="select-default bg-white">
                                    <select class="select-location" name="playground_statuses_id">
                                        @foreach($playground_statuses as $playground_status)
                                            <option
                                                value="{{ $playground_status->id }}">{{ $playground_status->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>

                    <!-- Contact -->

                    <div class="card-body p-6">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group mb-6">
                                    <label for="lat">Lat</label>
                                    <input type="text" class="form-control" name="lat" id="lat" placeholder="Lat">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group mb-6">
                                    <label for="lng">Lng</label>
                                    <input type="text" class="form-control" name="lng" id="lng" placeholder="Lng">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div id="playgroundMap" style="full-width; height: 450px"></div>
                            </div>

                        </div>
                    </div>

                    <div class="row justify-content-center">
                        <div class="col-md-7 col-lg-6 col-xl-5">
                            <div class="mb-6">
                                <button type="submit" class="btn btn-lg btn-primary btn-block">submit</button>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
            <br/>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

        </div>
    </section>
        
@endsection

@section('footerJS')
    <script src="{{ url('') }}/js/locationpicker.jquery.js"></script>
    <script>
        $(document).ready(function () {
            var latitude = 44.765345;
            var longitude = 17.197233;

            $('#playgroundMap').locationpicker({
                location: {
                    latitude: latitude,
                    longitude: longitude
                },
                //markerInCenter: true,
                inputBinding: {
                    latitudeInput: $('#lat'),
                    longitudeInput: $('#lng')
                },
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                styles: [],
                mapOptions: {},
                radius: 0
            });
        });
    </script>
@endsection
