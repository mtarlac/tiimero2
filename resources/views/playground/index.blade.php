@extends('layouts.page')

@section('caption', 'Playground')

@section('content')
    <!-- ====================================
———	LISTING HALF MAP LIST
===================================== -->
    <section class="main-contentiner map-half-content grid-two-items">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 order-lg-2 pl-lg-0">
                    <div class="inner-container">
                        <div class="map-lg-fixed">
                            <div class="map-container">
                                <div id="listing-main-map" class="map-half"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 px-xl-6">
                    <div class="row">
                        <div class="col-12">

                            <!-- List items goes here -->
                            @foreach($playgrounds as $playground)
                                <div class="card card-list card-listing" data-lat="{{ $playground->lat }}"
                                     data-lng="{{ $playground->lng }}" data-id="1">
                                    <div class="row">
                                        <div class="col-sm-5 col-xl-4">
                                            <div class="card-list-img">
                                                <img class="listing-img" src="{{ $playground->main_image }} "
                                                     alt="things-4">
                                            </div>
                                        </div>

                                        <div class="col-sm-7 col-xl-8">
                                            <div class="card-body p-0">
                                                <ul class="list-inline list-inline-rating">
                                                    <p class="text-primary">Latitude: {{ $playground->lat }} /
                                                        Longitude: {{ $playground->lng }}</p>
                                                    <p></p>
                                                </ul>

                                                <div class="d-flex justify-content-between align-items-center mb-1">
                                                    <h3 class="card-title listing-title mb-0">
                                                        <a href="/playgrounds/{{ $playground->id }}"> {{ $playground->name }} </a>
                                                    </h3>
                                                </div>
                                            </div>

                                            <span
                                                class="d-block mb-4 listing-address">{{ $playground->address }} </span>
                                            <span
                                                class="d-block mb-4 listing-address">{{ $playground->cities->name }} </span>
                                            <p class="mb-4">{{ $playground->description }} </p>
                                            <div>
                                                <button class="btn-like" data-toggle="tooltip" data-placement="top"
                                                        title="Playground Status">
                                                    <i class="text-primary"
                                                       aria-hidden="true">{{ $playground->playground_statuses->name }}</i>
                                                </button>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            @endforeach

                            <br/>

                            {{ $playgrounds->links() }}

                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection
