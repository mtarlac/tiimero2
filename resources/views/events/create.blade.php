@extends('layouts.page') @section('caption', 'Create events') @section('content')

<nav class="navbar navbar-expand-md navbar-dark">
    <div class="container">

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link " href="/dashboard">
                        <i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard <span class="sr-only">(current)</span></a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" href="javascript:void(0)" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-list-ul" aria-hidden="true"></i> Listing
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="/">My Listing</a>
                        <a class="dropdown-item" href="/">Add Listing</a>
                        <a class="dropdown-item" href="/">Edit Listing</a>
                    </div>
                </li>

                <li class="nav-item">
                    <a class="nav-link " href="/">
                        <i class="fa fa-envelope" aria-hidden="true"></i> Messages <span class="badge badge-success">12</span> </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link " href="/">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Booking Request </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link " href="/">
                        <i class="fa fa-eye" aria-hidden="true"></i> Reviews </a>
                </li>

            </ul>

            <form class="form-inline my-2 my-lg-0 position-relative d-none d-md-block">
                <input class="form-control-sm" placeholder="Search" aria-label="Search">
                <i class="fa fa-search" aria-hidden="true"></i>
            </form>
        </div>
    </div>
</nav>

<!-- ====================================
———	Add Listing
===================================== -->
<section class="bg-light py-5 height100vh">
    <div class="container">
        <nav class="bg-transparent breadcrumb breadcrumb-2 px-0 mb-5" aria-label="breadcrumb">
            <h2 class="font-weight-normal mb-4 mb-md-0">Submit Listings</h2>
            <ul class="list-unstyled d-flex p-0 m-0">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item"><a href="dashboard-list-admin.html">Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">Submit Listings</li>
            </ul>
        </nav>

        <form action="{{ route('events.store') }}" method="POST">
            @csrf

            <!-- About -->
            <div class="card">
                <div class="card-body px-6 pt-6 pb-1">
                    <h3 class="h4 mb-4">About</h3>
                    <p class="mb-5">We are not responsible for any damages caused by the use of this website, or by posting business listings here. Please use our site at your own discretion and exercise good judgement as well as common sense when advertising business here.</p>
                    <div class="row">
                        <div class="form-group col-md-6 mb-6">
                            <label for="eventName">Event Name</label>
                            <input type="text" class="form-control" id="eventName" name="name" placeholder="Event Name" required>
                        </div>

                        <div class="form-group col-md-3 mb-6">
                            <label for="startsAt">Starting time</label>
                            <input type="text" class="form-control" id="startsAt" name="starts_at" placeholder="Starts At" required>
                        </div>

                        <div class="form-group col-md-3 mb-6">
                            <label for="endsAt">Ending time</label>
                            <input type="text" class="form-control" id="endsAt" name="ends_at" placeholder="Ends At">
                        </div>

                        <div class="form-group col-md-12 mb-6">
                            <label for="description">Description</label>
                            <textarea class="form-control" rows="6" id="description" name="description" placeholder="Describe the event" required></textarea>
                        </div>

                        <div class="form-group col-md-4 mb-6">
                            <label for="no_of_players">Numbers of players</label>
                            <input type="text" class="form-control" id="no_of_players" name="no_of_players" placeholder="Number of players" required>
                        </div>

                        <div class="form-group col-md-4 mb-6">
                            <label for="openSlots">Open Slots</label>
                            <input type="text" class="form-control" id="openSlots" name="open_slots" placeholder="Open Slots" required>
                        </div>

                        <div class="form-group col-md-4 mb-6">
                            <label for="price">Price</label>
                            <input type="text" class="form-control" id="price" name="price" placeholder="Price" required>
                        </div>

                        <div class="form-group col-md-4 mb-6">
                            <label for="event_statuses_id">Event Status</label>
                            <div class="select-default bg-white">
                                <select class="select-location" name="event_statuses_id" required>

                                        @foreach($event_statuses as $event_status)
                                    <option value="{{ $event_status->id }}">{{ $event_status->name }}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group col-md-4 mb-6">
                            <label for="sport_types_id">Sport Type</label>
                            <div class="select-default bg-white">
                                <select class="select-location" name="sport_types_id" required>
                            
                                        @foreach($sport_types as $sport_type)
                                    <option value="{{ $sport_type->id }}">{{ $sport_type->name }}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group col-md-4 mb-6">
                            <label for="exampleFormControlSelect1">Event Type</label>
                            <div class="select-default bg-white">
                                <select class="select-location" name="event_types_id" required>

                                        @foreach($event_types as $event_type)
                                    <option value="{{ $event_type->id }}">{{ $event_type->name }}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="row justify-content-center">
                    <div class="col-md-7 col-lg-6 col-xl-5">
                        <div class="mb-6">
                            <button type="submit" id="contact-submit" class="btn btn-lg btn-primary btn-block">submit</button>
                        </div>
                    </div>
                </div>

                    @if ($errors->any())
                <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
                </div>
                    @endif

            </div>
        </form>
    </div>
</section>

</div>
<!-- element wrapper ends -->
@endsection

@section('footerJS')
   <script>
       $(document).ready(function () {
            $('#startsAt').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                timePicker: true,
                timePicker24Hour: true,
                timePickerIncrement: 15,
                minYear: 2000,
                locale: {
                    format: 'YYYY-MM-DD hh:mm'
                }
            });
        });
        
        $(document).ready(function () {
            $('#endsAt').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                timePicker: true,
                timePicker24Hour: true,
                timePickerIncrement: 15,
                minYear: 2000,
                locale: {
                    format: 'YYYY-MM-DD hh:mm'
                }
            });
        });
    </script>
@endsection