@extends('layouts.page')

@section('caption', 'Events')

@section('content')

<!-- ====================================
———	LISTING HALF MAP LIST
===================================== -->
    <section class="main-contentiner map-half-content grid-two-items">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 order-lg-2 pl-lg-0">
                    <div class="inner-container">
                        <div class="map-lg-fixed">
                            <div class="map-container">
                                <div id="listing-main-map" class="map-half"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 px-xl-6">
                    <div class="row">
                        <div class="col-12">

                        @foreach($events as $event)
                            <!-- List items goes here -->
                                <div class="card card-list card-listing" data-lat="-33.922125" data-lag="151.159277" data-id="1">
                                    <div class="row">
                                        <div class="col-sm-5 col-xl-4">
                                            <div class="card-list-img">
                                                <img class="listing-img" src="/assets/img/listing/listing-4.jpg" alt="things-4">
                                                <a href="/">
                                                    <span class="badge badge-primary">{{ $event->users->name }}</span>
                                                </a>
                                            </div>
                                        </div>

                                        <div class="col-sm-7 col-xl-8">
                                            <div class="card-body p-0">

                                                <ul class="list-inline list-inline-rating">
                                                    <li class="list-inline-item">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                    </li>

                                                    <li class="list-inline-item text-primary">
                                                        {{ $event->starts_at }}
                                                    </li>
                                                    <li class="list-inline-item">
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                    </li>
                                                    <li class="list-inline-item text-primary">
                                                        {{ $event->ends_at }}
                                                    </li>
                                                </ul>

                                                <div class="d-flex justify-content-between align-items-center mb-1">
                                                    <h3 class="card-title listing-title mb-4">
                                                        <a href="/events/{{ $event->id }}">{{ $event->name }}</a>
                                                    </h3>

                                                    <button class="btn-like px-2" data-toggle="tooltip"
                                                            data-placement="top" title="Numbers of players">
                                                        <i class="text-primary"
                                                           aria-hidden="true">{{ $event->no_of_players }}</i>
                                                    </button>

                                                    <button class="btn-like px-2" data-toggle="tooltip"
                                                            data-placement="top" title="Open slots">
                                                        <span>{{ $event->open_slots }}</span>
                                                    </button>
                                                </div>
                                            </div>

                                            <p class="mb-4">{{ $event->description }}</p>

                                            <div>
                                                <i class="text-primary" aria-hidden="true" data-toggle="tooltip"
                                                   data-placement="top" title="Event status">
                                                    {{ $event->event_statuses->name }} / </i>
                                                <i class="text-primary" aria-hidden="true" data-toggle="tooltip"
                                                   data-placement="top" title="Sport Type">
                                                    {{ $event->sport_types->name }} / </i>
                                                <i class="text-primary" aria-hidden="true" data-toggle="tooltip"
                                                   data-placement="top" title="Event type">
                                                    {{ $event->event_types->name }} </i>
                                            </div>

                                            <ul class="list-inline list-inline-rating">
                                                <li class="list-inline-item">
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                </li>
                                                <li class="list-inline-item text-primary">
                                                    {{ $event->created_at->format("d.M.Y. H:i") }}
                                                </li>
                                                <li class="list-inline-item">
                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                </li>
                                                <li class="list-inline-item text-primary">
                                                    {{ $event->updated_at }}
                                                </li>
                                            </ul>

                                            <form action="{{ route('join.event', $event->id) }}" method="POST">
                                                <!-- <input type="hidden" name="_method" value="PUT"> -->
                                                @csrf
                                                <!-- <a class="btn btn-primary pull-right px-6 py-2" href="#" role="button" id="buttonEvent {{ $event->id }}"
                                                    onclick="joinLeaveEvent( {{ $event->id }} );" return false>Join</a> -->
                                                    @if (!in_array($event->id, $userEvents))
                                                    <button class="btn btn-primary pull-right px-6 py-2 text-uppercase" id="buttonEvent{{ $event->id }}"
                                                        onclick="joinLeaveEvent( {{ $event->id }} ); return false;" type="submit" data-toggle="modal" data-target=".bd-example-modal-lg"> Join
                                                        </button>
                                                    @else
                                                    <button class="btn btn-danger pull-right px-6 py-2 text-uppercase" id="buttonEvent{{ $event->id }}"
                                                        onclick="joinLeaveEvent( {{ $event->id }} ); return false;"  type="submit" 
                                                        data-toggle="modal" data-target=".bd-example-modal-lg"> Leave
                                                        </button>
                                                    @endif
                                            </form>​
                                        </div>

                                    </div>
                                </div>
                            @endforeach

                            <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-sm" role="document">
                                  <div class="modal-content">
                                    <div id="messageDiv"></div>
                                  </div>
                                </div>
                            </div>

                            <br/>

                            {{ $events->links() }}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    </div> 
    <!-- element wrapper ends -->
@endsection

@section('footerJS')
    <script src="{{ url('') }}/js/helper.js"></script>
@endsection