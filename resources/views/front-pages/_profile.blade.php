@extends('layouts.page')

@section('caption', 'Profile')

@section('content')

<div class="main-wrapper">

  <!-- ====================================
———     NAVBAR USER BLACK
===================================== -->
  <nav class="navbar navbar-expand-md navbar-dark">
    <div class="container">

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link active" href="/">
              <i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard <span class="sr-only">(current)</span></a>
          </li>

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle " href="javascript:void(0)" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-list-ul" aria-hidden="true"></i> Listing
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="/">My Listing</a>
              <a class="dropdown-item" href="/">Add Listing</a>
              <a class="dropdown-item" href="/">Edit Listing</a>
            </div>
          </li>

          <li class="nav-item">
            <a class="nav-link " href="/">
              <i class="fa fa-envelope" aria-hidden="true"></i> Messages <span class="badge badge-success">12</span> </a>
          </li>

          <li class="nav-item">
            <a class="nav-link " href="/">
              <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Booking Request </a>
          </li>

          <li class="nav-item">
            <a class="nav-link " href="//l">
              <i class="fa fa-eye" aria-hidden="true"></i> Reviews </a>
          </li>

        </ul>

        <form class="form-inline my-2 my-lg-0 position-relative d-none d-md-block">
          <input class="form-control-sm" placeholder="Search" aria-label="Search">
          <i class="fa fa-search" aria-hidden="true"></i>
        </form>
      </div>
    </div>
  </nav>

  <!-- ====================================
        ———	MY PROFILE
        ===================================== -->
  <section class="bg-light">
    <div class="container">
      <nav class="bg-transparent breadcrumb breadcrumb-2 py-4" aria-label="breadcrumb">
        <h2 class="font-weight-normal mb-4 mb-md-0">My Profile</h2>
        <ul class="list-unstyled d-flex p-0 m-0">
          <li class="breadcrumb-item"><a href="/home">Home</a></li>
          <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="/tiimero">My Tiimero </a></li>

        </ul>
      </nav>
      <div class="row">
        <div class="col-md-5 col-lg-4">

          <div class="card card-profile">
            <div class="card-body text-center">
              <p>Your Current Plan</p>
              <h3 class="font-weight-normal mb-4">Platinum Package</h3>
              <span class="d-block mb-4">Next Payment: <span class="text-primary">15/01/2017</span></span>
              <a href="/page/pricing" class="btn btn-secondary btn-sm mb-3">Change</a>
            </div>
          </div>

        </div>

        <div class="col-md-7 col-lg-8">

          <form method="POST" action="{{ route('update.profile') }}">
          <input type="hidden" name="_method" value="PUT">
            @csrf

            <div class="card">
              <div class="card-body p-6">
                <h3 class="font-weight-normal mb-4">About Me</h3>

                <div class="row">
                  <div class="form-group col-md-6">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{ $user->name }}">
                  </div>
                  <div class="form-group col-md-6">
                    <label for="email">E-Mail</label>
                    <input type="email" class="form-control"  id="email" name="email" placeholder="Email" value="{{ $user->email }}">
                  </div>
                </div>

                <div class="mt-4">
                  <button class="btn btn-primary text-uppercase" type="submit">
                    Save Change
                  </button>
                </div>
                <br/>

                @if(session('status'))
                  <div class="alert alert-success">
                    {{ session('status') }}
                  </div>
                @endif

                @if(session('error'))
                <div class="alert alert-danger">
                  {{ session('error') }}
                </div>
              @endif

              </div>
            </div>
          </form>

        </div>
      </div>
    </div>
  </section>

  <!-- ====================================
——— DASHBOARD LIST ADMIN
===================================== -->
  <section class="bg-light py-5">
    <div class="container">

      <div class="row">
        <!-- Listings -->
        <div class="col-md-4">
          <div class="card">
            <div class="card-header bg-white">
              <span class="text-content">Playgrounds</span>
            </div>

            <div class="card-body widget-content">
              <span class="widget-content-title">21</span>
              <a href="/playgrounds" class="btn btn-link">All Playgrounds</a>
              <a href="/playgrounds/create"><button class="btn ml-8 px-3 py-1 btn-sm btn-success">Create
                </button></a>


            </div>
          </div>
        </div>

        <!-- Visits -->
        <div class="col-md-4">
          <div class="card">
            <div class="card-header bg-white">
              <span class="text-content">Courts</span>
            </div>

            <div class="card-body widget-content">
              <span class="widget-content-title">203</span>
              <a href="/courts" class="btn btn-link">All Courts</a>
              <a href="/courts/create"><button class="btn ml-9 px-3 py-1 btn-sm btn-success">Create
                </button></a>

            </div>
          </div>
        </div>

        <!-- Booking -->
        <div class="col-md-4">
          <div class="card">
            <div class="card-header bg-white">
              <span class="text-content">Sports Events</span>
            </div>

            <div class="card-body widget-content">
              <span class="widget-content-title">87</span>
              <a href="/events" class="btn btn-link">All Sports Events</a>
              <a href="/events/create"><button class="btn ml-8 px-3 py-1 btn-sm btn-success">Create
                </button></a>

            </div>
          </div>
        </div>

      </div>

    </div><!-- end row -->

</div>
</section>

<!-- ====================================
———	Booking Requests
===================================== -->
	<section class="pb-8 bg-light">
	  <div class="container">

		<nav class="bg-transparent breadcrumb breadcrumb-2 px-0 mb-5" aria-label="breadcrumb">
		  <h2 class="font-weight-normal mb-4 mb-md-0">My Playgrounds</h2>
		</nav>

		<table id="booking" class="display nowrap table-data-default" style="width:100%">
		  <thead>
			<tr>
			  <th>Image</th>
			  <th>Playground Name</th>
			  <th>Description</th>
			  <th>Playground Status</th>
			  <th>Address</th>
			  <th>Cities</th>
			  <th>Lat</th>
			  <th>Lng</th>
			  <th data-priority="2"></th>
			</tr>
		  </thead>
		  <tbody>
				@foreach($playgrounds as $playground)
			<tr>
			  <td>
				<img class="img-sm rounded" src="{{ $playground->image }}" alt="Image">
			  </td>
			  <td class="text-capitalize">
				<a class="text-hover-primary text-muted" href="/playgrounds/{{ $playground->id }}">{{ $playground->name }}</a>
			  </td>
			  <td class="text-capitalize">{{ $playground->description }}</td>
			  <td>
				<span class="d-block">{{ $playground->playground_statuses->name }}</span>
			  </td>
			  <td>
				<a class="text-muted font-weight-bold text-hover-primary" href="user-profile.html">{{ $playground->address }}</a>
		      </td>
			  <td>
				<span class="d-block ">{{ $playground->cities->name }}</span>
			  </td>
			  <td>
				<span class="badge badge-warning px-2 py-1">{{ $playground->lat }}</span>
			  </td>
			  <td>
				<span class="badge badge-warning px-2 py-1">{{ $playground->lng }}</span>
			  </td>
			  <td>
				<div class="row">
				  <div class="col-12 col-sm-6">
					<a href="/playgrounds/{{ $playground->id }}/edit"><button class="btn px-4 py-1 ml-4 btn-sm btn-primary">Edit</button></a>
				  </div>
				  <div class="col-12 col-sm-6">
					<form action="/playgrounds/{{ $playground->id }}" method="POST">
					  <input type="hidden" name="_method" value="DELETE">
					  @csrf
					  <button class="btn px-3 py-1 btn-sm btn-danger" onclick="return confirm('Are you sure you want to delete this data')">Delete
					  </button>
					</form>
				  </div>
				</div>
			  </td>
			</tr>
				@endforeach

			</tbody>
	    </table>
        
            <br/>
            @if ($user->user_types_id == 1)
                {{ $playgrounds->links() }}
            @else
            @endif

      </div>

      
    </section>

    </div> <!-- element wrapper ends -->
@endsection