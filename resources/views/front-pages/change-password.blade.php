@extends('layouts.page') 

@section('caption', 'Change Password') 

@section('content')
<!-- ====================================
———	CHANGE PASSWORD
===================================== -->
<section class="py-7 py-md-10 bg-light ">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-5">
                <div class="card">
                    <div class="bg-primary text-center py-4">
                        <h2 class="text-white mb-0 h4">{{ __('Update Password') }}</h2>
                    </div>


                    <div class="card-body p-6">
                        <form method="POST" action="{{ route('change.password') }}">
                            @csrf
<!-- Correct error bag to be uniqe as in other views -->
                            @foreach ($errors->all() as $error)
                                <p class="text-danger">{{ $error }}</p>
                            @endforeach

                            <div class="row">
                                <div class="form-group col-12">
                                    <label for="currentPassword">{{ __('Current Password') }}*</label>
                                    <input id="currentPassword" type="password" class="form-control" name="current_password" placeholder="********" 
                                        autocomplete="current-password">
                                </div>

                                <div class="form-group col-12">
                                    <label for="newPassword">{{ __('New Password') }}*</label>
                                    <input id="newPassword" type="password" class="form-control" name="new_password" placeholder="New Password" 
                                        autocomplete="new-password">
                                </div>

                                <div class="form-group col-12">
                                    <label for="confirmPassword">{{ __('Confirm Password') }}*</label>
                                    <input id="confirmPassword" type="password" class="form-control" name="new_confirm_password" placeholder="Confirm Password" 
                                        autocomplete="new-password">
                                </div>

                                <div class="form-group col-12 mt-4">
                                    <button class="btn btn-outline-primary" type="submit">{{ __('Change Password') }}</button>
                                </div>
                            </div>                          
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
@endsection