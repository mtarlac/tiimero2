@extends('layouts.page')

@section('caption', 'Contact Us')

@section('content')
    <!-- ====================================
———	BREADCRUMB
===================================== -->
    <section class="py-8 bg-no-repeat bg-cover"
             style="background-image: url(/assets/img/background/breadcrumb-bg.jpg); ">
        <div class="container">
            <div class="d-flex flex-column justify-content-center align-items-center">
                <nav aria-label="breadcrumb">
                    <h2 class="breadcrumb-title text-center text-white mb-4">Contact Us</h2>
                    <ul class="breadcrumb bg-transparent justify-content-center p-0 m-0">
                        <li class="breadcrumb-item"><a class="text-white" href="/home">Home</a></li>
                        <li class="breadcrumb-item text-white active" aria-current="page">Contact Us</li>
                    </ul>
                </nav>
            </div>
        </div>
    </section>

    <!-- Grid wrapper for ell elements -->
    <div class="element-wrapper">

        <!-- ====================================
        ———	contact-us
        ===================================== -->

        <section class="py-7 py-md-10">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 col-lg-4">
                        <!-- Map -->
                        <div class="map-sidebar mb-5">
                            <div id="simple-map"
                                 style="position:relative; margin: 0;padding: 0;height: 538px; max-width: none;"></div>
                        </div>

                        <!-- Address -->
                        <ul class="list-unstyled">
                            <li class="d-flex align-items-start mb-3">
                                <i class="fa fa-map-marker mr-3 mt-1" aria-hidden="true"></i>
                                <span>2880 Broadway, New York, NY 10025, USA</span>
                            </li>
                            <li class="d-flex align-items-start mb-3">
                                <i class="fa fa-phone mr-3 mt-1" aria-hidden="true"></i>
                                <div>
                                    <a class="text-muted" href="tel:+55 654 545 122">+55 654 545 122</a> <br>
                                    <a class="text-muted" href="tel:+55 654 545 123">+55 654 545 123</a>
                                </div>
                            </li>
                            <li class="d-flex align-items-start mb-3">
                                <i class="fa fa-envelope mr-3 mt-1" aria-hidden="true"></i>
                                <div>
                                    <a class="text-muted" href="mailto:info@example.com">info@example.com</a>
                                    <a class="text-muted" href="mailto:info@startravelbangladesh.com">info@startravelbangladesh.com</a>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <!-- Get inTouch -->
                    <div class="col-md-7 col-lg-8">
                        <h2 class="font-weight-normal my-4 mt-md-0">Get inTouch</h2>
                        <p class="mb-6">Please feel free to contact us if you have queries, require more information or
                            have any other request.</p>
                        <hr>

                        <form class="pt-4" method="POST" action="{{ url('') }}/sendMessage" id="contactForm">
                           {{ csrf_field() }}
                           <div class="row">
                               <!-- Subject -->
                               <div class="form-group col-md-6 mb-6">
                                   <label for="inputState">Subject*</label>
                                   <div class="select-bg-transparent select-border w-100">
                                       <select class="select-location">
                                           <option>-- Select Subject --</option>
                                           <option>I need Help</option>
                                           <option>Payment question</option>
                                           <option>Abuse report</option>
                                       </select>
                                   </div>
                               </div>
                               <!-- Company Name -->
                               <div class="form-group col-md-6 mb-6">
                                   <label for="companyName">Company Name</label>
                                   <input type="text" class="form-control" id="companyName" name="companyName">
                               </div>
                           </div>
                           <!-- Your Name -->
                           <div class="form-group mb-6">
                               <label for="yourName">Your Name*</label>
                               <input type="text" class="form-control" id="yourName" name="yourName" required>
                           </div>
                           <!-- Email Address -->
                           <div class="form-group mb-6">
                               <label for="emailAddress">Email Address*</label>
                               <input type="email" class="form-control" id="emailAddress" name="emailAddress" required>
                           </div>
                           <!-- Text -->
                           <div class="form-group mb-6">
                               <label for="textBox" class="control-label">Text*</label>
                               <textarea class="form-control" rows="6" name="textBox" id="textBox" required></textarea>
                           </div>
                           <!-- Button -->
                           <button type="submit" class="btn btn-primary text-uppercase px-5 py-3" id="submitContactForm">send now</button>
                       </form>
                       <br/>
                        <div id="messageDiv"></div>
                    </div>
                </div>
            </div>
        </section>

    </div>
    <!-- element wrapper ends -->

@endsection

@section('footerJS')
   <script src="{{ url('') }}/js/helper.js"></script>
   <script>
       $(document).ready(function () {
           $("#submitContactForm").click(function () {
               var valid = nValidatorReq(["yourName", "emailAddress", "textBox"]);
               if (valid) {
                   $.ajax({
                       type: "POST",
                       url: "{{ url('') }}/sendMessage",
                       data: {
                           "_token": document.getElementsByName("_token")[0].value,
                           "companyName": $("#companyName").val(),
                           "yourName": $("#yourName").val(),
                           "emailAddress": $("#emailAddress").val(),
                           "textBox": $("#textBox").val()
                       },
                       dataType: "json",
                       success: function (data) {
                           var htmlVar = '';
                           $("#messageDiv").html('');
                           $("#messageDiv").removeClass("alert alert-danger");
                           $("#messageDiv").addClass("alert alert-success");
                           $.each(data, function (key, value) {
                               htmlVar += value + '<br/>';
                           });
                           $("#messageDiv").html(htmlVar);
                           $('#contactForm')[0].reset();
                           document.getElementsByName("_token")[0].value = "{{ csrf_token() }}";
                       },
                       error: function (data) {
                           var htmlVar = '';
                           var errors = data.responseJSON;
                           $("#messageDiv").html('');
                           $("#messageDiv").removeClass("alert alert-success");
                           $("#messageDiv").addClass("alert alert-danger");
                           $.each(errors, function (key, value) {
                               htmlVar += value + '<br/>';
                           });
                           $("#messageDiv").html(htmlVar);
                       }
                   });
               }
               return false;
           });
       });
       //};
   </script>
@endsection



