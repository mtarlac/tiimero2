@extends('layouts.page')

@section('caption', 'Email')

@section('content')
    <!-- ====================================
———	LOGIN PAGE
===================================== -->
    <section class="py-7 py-md-10 bg-light ">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6 col-lg-5">
                    <div class="card">
                        <div class="bg-primary text-center py-4">
                            <h2 class="text-white mb-0 h4">{{ __('Reset Password') }}</h2>
                        </div>
                        <div class="card-body px-7 pt-7 pb-0">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <form method="POST" action="{{ route('password.email') }}">
                                @csrf

                                <div class="form-group mb-7">
                                    <label for="email">{{ __('E-Mail Address') }}</label>
                                    <input id="email" name="email" type="text" class="form-control" required
                                           aria-describedby="emailHelp">

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group d-flex justify-content-between align-items-center mb-7">
                                    <button type="submit" class="btn btn-outline-primary text-uppercase">
                                        {{ __('Send Password Reset Link') }}
                                    </button>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection

