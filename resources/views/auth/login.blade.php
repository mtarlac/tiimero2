@extends('layouts.page')

@section('caption', 'Log In')

@section('content')
    <!-- ====================================
———	LOGIN PAGE
===================================== -->
    <section class="py-7 py-md-10 bg-light ">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6 col-lg-5">
                    <div class="card">
                        <div class="bg-primary text-center py-4">
                            <h2 class="text-white mb-0 h4">Members Log In</h2>
                        </div>
                        <div class="card-body px-7 pt-7 pb-0">

                        @if (count($errors) > 0)

                            <!-- Fix later for input field to show the error-->
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <form method="POST" action="{{ route('login') }}">
                                @csrf

                                <div class="form-group mb-7">
                                    <label for="email">{{ __('E-Mail Address') }}</label>
                                    <input id="email" name="email" type="text" class="form-control" required
                                           aria-describedby="emailHelp">

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                                    @enderror
                                    <em>Enter your Foundation email.</em>
                                </div>
                                <div class="form-group mb-7">
                                    <label for="password">{{ __('Password') }}*</label>
                                    <input id="password" name="password" type="password" class="form-control" required>
                                    <em>Enter the password that accompanies your e-mail.</em>

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                      </span>
                                    @enderror
                                </div>
                                <div class="form-group d-flex justify-content-between align-items-center mb-7">
                                    <button type="submit" class="btn btn-outline-primary text-uppercase">
                                        {{ __('Login') }}
                                    </button>
                                    @if (Route::has('password.request'))
                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                            {{ __('Forgot Your Password?') }}
                                        </a>
                                    @endif
                                </div>
                            </form>
                        </div>
                        <div class="card-footer bg-transparent text-center py-3">
                            <p class="mb-0">Not a member yet? <a href="/register" class="link">{{ __('Register!') }}</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
